<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Web | Media Online</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
        <p><label>First Name:</label> </p>
        <p>
            <input type="text" name="firstname" placeholder="First Name..." />
        </p>
        <p><label>Last Name:</label></p>
        <p>
            <input type="text" name="lastname" placeholder="Last Name..." />
        </p>
        <p><label>Gender:</label></p>
        <p><label><input type="radio" name="jenis_kelamin" value="Male" /> Male</label></p>
        <p><label><input type="radio" name="jenis_kelamin" value="Female" /> Female</label></p>
        <p><label><input type="radio" name="jenis_kelamin" value="others" /> Others</label></p>
        <label>Language Spoken:</label></p>
        <p><label><input type="checkbox" name="language" value="bahasaindonesia" /> Bahasa Indonesia</label></p>
        <p><label><input type="checkbox" name="language" value="english" /> English</label></p>
        <p><label><input type="checkbox" name="jenis_kelamin" value="other" /> Other</label></p>
        <p>
            <label>Nationality:</label>
            <select name="agama">
                <option value="indonesian">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="US">US</option>
                <option value="England">England</option>
            </select>
        </p>
        <p><label>Bio:</label></p>
        <p><textarea name="biografi"></textarea></p>
        <p><input type="submit" name="signup" value="Sign Up" onclick="document.location='/welcome'"/></p>
</body>
</html>