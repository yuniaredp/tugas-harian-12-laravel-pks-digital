<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('halaman.welcome2');
});

Route::get('/', function () {
    return view('halaman.index');
});

Route::get('/form', function () {
    return view('halaman.form');
});
